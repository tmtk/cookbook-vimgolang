# @see http://mattn.kaoriya.net/software/vim/20130531000559.htm

vimrc_path = "#{ENV['HOME']}/.vimrc"

ruby_block ".vimrc" do
  block do
    vimrc = open(vimrc_path) {|f| f.readlines }

    filetype = "filetype plugin indent on"
    vimrc.unshift filetype unless vimrc.include? filetype

    srcline = "source ~/.vimrc.golang\n"
    vimrc << srcline unless vimrc.include? srcline

    open(vimrc_path, "w") {|f| f.write vimrc.join "" }
  end
end

ruby_block ".vimrc.golang" do
  block do
    open("#{ENV['HOME']}/.vimrc.golang", "w") {|f|
      f.write <<EOF
" golang settings
set rtp+=$GOROOT/misc/vim
exe "set rtp+=".globpath($GOPATH, "src/github.com/nsf/gocode/vim")
runtime! ftdetect/*.vim
EOF
    }
  end
end

ruby_block "gocode" do
  block do
    system("go get github.com/nsf/gocode")
  end
end
